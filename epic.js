"use script";

!(function() {
  let dateCache = {};
  let imageCache = {};
  let infoCache = {};

  function setup(e) {
    const form = document.querySelector("#request_form");
    const type = document.querySelector("#type");
    const date = document.querySelector("#date");
    let dateChosen = document.querySelector("#date").value;
    const imgMenu = document.querySelector("#image-menu");
    const temp = document.querySelector("#image-menu-item");
    let collection = document.querySelector("#type").value;
    const allCollection = ['natural', 'enhanced', 'aerosol', 'cloud'];
    const select = document.querySelector("#type");
    let option = document.createElement("option");
    
    for(let imageType of allCollection) {
      option.value = imageType;
      option.textContent = imageType;
      select.appendChild(option);
      option = document.createElement("option");

      dateCache[`${imageType}`] = null;
      imageCache[`${imageType}`] = new Map();
      infoCache[`${imageType}`] = new Map();
    }

    let li = temp.content.firstElementChild.cloneNode(true);
    let imageURLs = [];
    let imageInfo = [];
    
    getMaxDate();

    let EpicApplication = {
      images: imageURLs,
      imageType: collection,
      dateCache: dateCache,
      imageCache: imageCache,
      infoCache: infoCache
    };    

    function fetchData(e) {
      e.preventDefault();
      collection = document.querySelector("#type").value;
      imageURLs = [];
      imageInfo = [];
      dateChosen = document.querySelector("#date").value;
      let url = `https://epic.gsfc.nasa.gov/api/${collection}/date/${dateChosen}`
      let date = dateChosen.replaceAll("-", "/");

      if (!imageCache[collection].has(date)) {
        fetch(url)
        .then(response => { 
          if (!response.ok) {
            throw new Error("Not 2xx response", {cause: response});
          }
          else return response.json(); 
        })
        .then(data => {
          data
          .map((e, i ,arr) => {
            imageURLs.push(
              `https://epic.gsfc.nasa.gov/archive/${collection}/${date}/jpg/` + arr[i].image + ".jpg"
            )
            imageInfo.push({
              caption: arr[i].caption, date: arr[i].date
            })
          })
          imageCache[collection].set(date, imageURLs);
          EpicApplication["imageCache"] = imageCache
          infoCache[collection].set(date, imageInfo);
          EpicApplication["infoCache"] = infoCache;
          EpicApplication["images"] = EpicApplication["imageCache"][collection].get(date);
          EpicApplication["imageType"] = collection; 
          makeList(EpicApplication, date);
        })
        .catch(error => {
          imgMenu.textContent = '';
          errorMessage();
        })
      }
      if (imageCache[collection].has(date)) {
        EpicApplication["images"] = EpicApplication["imageCache"][collection].get(date);
        EpicApplication["imageType"] = collection; 
        makeList(EpicApplication, date);
      }
    }
    form.addEventListener("submit", fetchData);

    function makeList(EpicApplication, dateChosen) {
      imgMenu.textContent = ''; // clears all li elements when creating new ones.
      let typeChosen = EpicApplication["imageType"];
      
      for (let i = 0; i < EpicApplication["imageCache"][typeChosen].get(dateChosen).length; i++) {
        li.children[0].textContent = EpicApplication["infoCache"][typeChosen].get(dateChosen)[i].date;
        li.setAttribute("data-image-list-index", i);
        imgMenu.appendChild(li);
        li = li.cloneNode(true);
      }  

      // An if statement if there is no images to show.
      if (EpicApplication["imageCache"][typeChosen].get(dateChosen).length === 0) {
        errorMessage();
      }
    }
    
    const earthImg = document.querySelector("#earth-image");
    const earthImgDate = document.querySelector("#earth-image-date");
    const earthImgTitle = document.querySelector("#earth-image-title");

    function displayImage(e) {
      // An if statement to avoid errors when ul is somehow "clicked"
      if (e.target.tagName != 'UL') {
        let date = dateChosen.replaceAll("-", "/");
        let index = e.target.closest('li').getAttribute("data-image-list-index");
        let typeChosen = EpicApplication["imageType"];
        let imgInfo = EpicApplication["infoCache"][typeChosen].get(date);

        if (EpicApplication["imageCache"][typeChosen].has(date)){
          // An if statement to avoid showing an image and its details when clicking the error li element.
          if (EpicApplication["imageCache"][typeChosen].get(date).length != 0) {
            earthImg.src = EpicApplication["images"][index];
            earthImg.alt = imgInfo[index].caption;
            earthImgDate.textContent = imgInfo[index].date;
            earthImgTitle.textContent = imgInfo[index].caption; 
          }
        }
      }
    }
    imgMenu.addEventListener("click", displayImage);

    function getMaxDate() {
      collection = document.querySelector("#type").value;     
      let dateURL = `https://epic.gsfc.nasa.gov/api/${collection}/all` 
      if (dateCache[collection] == null) {
        fetch(dateURL)
        .then(response => { if (!response.ok) {
            throw new Error("Not 2xx response", {cause: response});
          }
          else return response.json(); 
        })
        .then(data => {
          date.max = data[0].date;
          dateCache[collection] = date.max;
          if (date.value == '') {
            date.value = data[0].date;
          }
        })
        .catch(error => {
          errorMessage();
        })
      }
      else {
        date.max = dateCache[collection];
      }
    }
    type.addEventListener("change", getMaxDate);

    const errorLi = document.createElement("li");

    function errorMessage() {
      errorLi.textContent = "Error! No images to show."
      imgMenu.appendChild(errorLi);
    }
  }
  document.addEventListener("DOMContentLoaded", setup);
})();